import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRecipientComponent } from './home-recipient.component';

describe('HomeRecipientComponent', () => {
  let component: HomeRecipientComponent;
  let fixture: ComponentFixture<HomeRecipientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeRecipientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRecipientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
