import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRecipientComponent } from './home-recipient/home-recipient.component';



@NgModule({
  declarations: [HomeRecipientComponent],
  imports: [
    CommonModule
  ]
})
export class RecipientModule { }
