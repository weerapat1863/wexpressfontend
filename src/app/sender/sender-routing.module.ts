import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookCarComponent } from './components/book-car/book-car.component';
import { SenderComponent } from './sender.component';
import { BookComponent } from './components/book/book.component';
import { BookHistoryComponent } from './components/book-history/book-history.component';

const routes: Routes = [
  {
    path: '',
    component: SenderComponent,
    children: [
      { path: 'book-car', component: BookCarComponent },
      { path: 'book', component: BookComponent },
      { path: 'book-history', component: BookHistoryComponent },
      { path: 'book-car/:senderId', component: BookCarComponent },
      { path: '**', redirectTo: 'book-car' }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SenderRoutingModule { }