import { Component, OnInit } from '@angular/core';
import { SenderService } from '../../../services/sender/sender.service';
import { ActivatedRoute } from '@angular/router';
// import GoogleMapsModule from '@angular/google-maps'

@Component({
  selector: 'app-book-car',
  templateUrl: './book-car.component.html',
  styleUrls: ['./book-car.component.scss']
})
export class BookCarComponent implements OnInit {
  public sender_address = '';
  public sender_datetime: Date = new Date();
  public sender_name: string = '';
  public sender_id_passport: Number | null = null;
  public sender_tel: Number | null = null;
  public sender_email: string = '';

  senderId = 0;

  zoom = 12
  center: google.maps.LatLngLiteral | undefined;
  options: google.maps.MapOptions = {
    mapTypeId: 'hybrid',
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    maxZoom: 15,
    minZoom: 8,
  }
  constructor(
    private senderService: SenderService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.senderId = Number(routeParams.get("senderId"));
    console.log('senderId : ', this.senderId)
    if(this.senderId != 0){
      this.senderService.getSenderById(this.senderId).subscribe(res => {
        console.log(res)
        if (res != null) {
          this.sender_address = res.senderAddress;
          this.sender_datetime = new Date(res.senderDatetime);
          this.sender_name = res.senderName;
          this.sender_id_passport = res.senderIdPassport;
          this.sender_tel = res.senderTel;
          this.sender_email = res.senderEmail;
        }
      })
    }
    navigator.geolocation.getCurrentPosition((position) => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      }
    })
  }

  saveSender() {
    this.senderService.sender(
      this.sender_address,
      this.sender_datetime,
      this.sender_name,
      this.sender_id_passport,
      this.sender_tel,
      this.sender_email
    ).subscribe(res => {
      console.log(res)
    });
  }

}
