import { NgModule } from '@angular/core';
import { SenderComponent } from './sender.component';
import { BookCarComponent } from './components/book-car/book-car.component';
import { FormsModule } from '@angular/forms';
import { SenderRoutingModule } from './sender-routing.module';
import { BookComponent } from './components/book/book.component';
import { BookHistoryComponent } from './components/book-history/book-history.component';
import { GoogleMapsModule } from '@angular/google-maps'
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    SenderComponent,
    BookCarComponent,
    BookComponent,
    BookHistoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SenderRoutingModule,
    GoogleMapsModule
  ],
})
export class SenderModule {}
