import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeDriverComponent } from './components/home-driver/home-driver.component';
import { DriverRoutingModule } from './driver-routing.module';
import { StatusDriverComponent } from './components/status-driver/status-driver.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HomeDriverComponent,
    StatusDriverComponent
  ],
  imports: [DriverRoutingModule, CommonModule, FormsModule],
})
export class DriverModule {}
