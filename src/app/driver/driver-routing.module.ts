import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DriverComponent } from './driver.component';
import { HomeDriverComponent } from './components/home-driver/home-driver.component';
import { StatusDriverComponent } from './components/status-driver/status-driver.component';

const routes: Routes = [
  {
    path: '',
    component: DriverComponent,
    children: [
      { path: 'home-driver', component: HomeDriverComponent },
      { path: 'status-driver', component: StatusDriverComponent },
      { path: '**', redirectTo: 'home-driver' }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DriverRoutingModule { }