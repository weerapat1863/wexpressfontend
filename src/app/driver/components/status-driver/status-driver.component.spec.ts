import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusDriverComponent } from './status-driver.component';

describe('StatusDriverComponent', () => {
  let component: StatusDriverComponent;
  let fixture: ComponentFixture<StatusDriverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatusDriverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
