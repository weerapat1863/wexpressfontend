import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { AdminModule } from './admin/admin.module';
import { CommonModule } from '@angular/common';
import { SenderModule } from './sender/sender.module';
import { DriverModule } from './driver/driver.module';
import { DriverComponent } from './driver/driver.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DriverComponent,
  ],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AdminModule,
    CommonModule,
    SenderModule,
    DriverModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
