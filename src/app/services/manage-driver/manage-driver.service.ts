import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Driver } from 'src/app/interfaces/driver';

@Injectable({
  providedIn: 'root'
})
export class ManagerDriverService {
  constructor(private http: HttpClient) { }
  getConfig():Observable<any> {
    return this.http.get<any>('https://localhost:44398/api/drivers');
  }
  driver( driver_code: string ,  vehicle_registration_number:Number | null,driver_name: string ,driver_tel:Number | null,driver_model: string,driver_address: string ): Observable<any> {
    let headers = new HttpHeaders({
      'Content-Type':  'application/json',
    })
    let body = {
      DriverCode: driver_code,
      VehicleRegistrationNumber: vehicle_registration_number,
      DriverName: driver_name,
      DriverTel: driver_tel,
      DriverModel: driver_model,
      DriverAddress: driver_address
    }
    console.log(body)
    return this.http.post<any>('https://localhost:44398/api/v1/drivers',body,{headers:headers});

  }

  getDriverById(id:number):Observable<Driver>{
    return this.http.get<Driver>(`${environment.api}api/v1/drivers/${id}`);
  }

  getList(): Observable<Driver[]> {
    return this.http.get<Driver[]>(`${environment.api}api/v1/drivers`);
  }

  delDriver(): Observable<Driver>{
    return this.http.delete<Driver>(`${environment.api}api/v1/drivers`);
  }

}