import { TestBed } from '@angular/core/testing';

import { ManagerDriverService } from './manage-driver.service';

describe('ManagerDriverService', () => {
  let service: ManagerDriverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManagerDriverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
