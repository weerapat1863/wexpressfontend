import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }
  getConfig():Observable<any> {
    return this.http.get<any>('https://localhost:44398/api/users');
  }

  login(username: string, password: string): Observable<boolean> {
    let headers = new HttpHeaders({
      'Content-Type':  'application/json',
    })
    let body = {
      username: username,
      password:password
    }
    return this.http.post<boolean>('https://localhost:44398/api/v1/users/login',body,{headers:headers});

  }
}
