import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Sender } from 'src/app/interfaces/sender';

@Injectable({
  providedIn: 'root'
})
export class SenderService {

  constructor(private http: HttpClient) { }
  getConfig():Observable<any> {
    return this.http.get<any>('https://localhost:44398/api/senders');
  }

  sender(sender_address: string, sender_datetime: Date,sender_name: string,sender_id_passport:Number | null,sender_tel:Number | null,sender_email:string): Observable<any> {
    let headers = new HttpHeaders({
      'Content-Type':  'application/json',
    })
    let body = {
      SenderAddress: sender_address,
      SenderDatetime: sender_datetime,
      SenderName: sender_name,
      SenderIdPassport: sender_id_passport,
      SenderTel: sender_tel,
      SenderEmail: sender_email
    }
    console.log(body)
    return this.http.post<any>('https://localhost:44398/api/v1/senders',body,{headers:headers});
  }

  getSenderById(id:number):Observable<Sender>{
    return this.http.get<Sender>(`${environment.api}api/v1/senders/${id}`);
  }
}




