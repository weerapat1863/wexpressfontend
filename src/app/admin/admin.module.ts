import { NgModule } from '@angular/core';

import { AdminRoutingModule } from './admin-routing.module';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './components/nav/nav.component';
import { AdminComponent } from './admin.component';
import { ManageParcelComponent } from './components/manage-parcel/manage-parcel.component';
import { ManagePreemptionComponent } from './components/manage-preemption/manage-preemption.component';
import { ManageDriverComponent } from './components/manage-driver/manage-driver.component';
import { SummaryComponent } from './components/summary/summary.component';
import { TrackingComponent } from './components/tracking/tracking.component';
import { TrackStatusComponent } from './components/track-status/track-status.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    AdminComponent,
    HomeComponent,
    NavComponent,
    ManageParcelComponent,
    ManagePreemptionComponent,
    ManageDriverComponent,
    SummaryComponent,
    TrackingComponent,
    TrackStatusComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
