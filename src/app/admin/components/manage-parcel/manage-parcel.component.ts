import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manage-parcel',
  templateUrl: './manage-parcel.component.html',
  styleUrls: ['./manage-parcel.component.scss']
})
export class ManageParcelComponent implements OnInit {
  public name:string = "";
  constructor() { }

  ngOnInit(): void {
    this.name = ""
  }

}
