import { Component, OnInit } from '@angular/core';
import { ManagerDriverService } from '../../../services/manage-driver/manage-driver.service';
import { ActivatedRoute } from '@angular/router';
import { Driver } from 'src/app/interfaces/driver';

@Component({
  selector: 'app-manage-driver',
  templateUrl: './manage-driver.component.html',
  styleUrls: ['./manage-driver.component.scss']
})
export class ManageDriverComponent implements OnInit {
  listDriver : Driver[] = [];
  public driver_id !: number;
  public driver_code: string = '';
  public vehicle_registration_number: Number | null = null;
  public driver_name: string = '';
  public driver_tel: number | null = null;
  public driver_model: string = '';
  public driver_address: string = '';

  constructor(private managerDriverService: ManagerDriverService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.driver_id = Number(routeParams.get("driverId"));
    console.log('driverId : ', this.driver_id)
    if (this.driver_id != 0) {
      this.managerDriverService.getDriverById(this.driver_id).subscribe(res => {
        console.log(res)
        if (res != null) {
          this.driver_code = res.driverCode;
          this.vehicle_registration_number = res.vehicleRegistrationNumber;
          this.driver_name = res.driverName;
          this.driver_tel = res.driverTel;
          this.driver_model = res.driverModel;
          this.driver_address = res.driverAddress;
        }
      })
    } else {
      this.managerDriverService.getList().subscribe(res => {
        this.listDriver = res;
        console.log(this.listDriver)
      })
    }
    }
    saveDriver() {
      this.managerDriverService.driver(
        this.driver_code,
        this.vehicle_registration_number,
        this.driver_name,
        this.driver_tel,
        this.driver_model,
        this.driver_address
      ).subscribe(res => {
        console.log(res)
      });
    }
    delDriver(id: Int32Array) {
      this.managerDriverService.driver(
        this.driver_code,
        this.vehicle_registration_number,
        this.driver_name,
        this.driver_tel,
        this.driver_model,
        this.driver_address
      ).subscribe(res => {
        console.log(res)
      });
    }
}
