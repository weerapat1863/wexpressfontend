import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePreemptionComponent } from './manage-preemption.component';

describe('ManagePreemptionComponent', () => {
  let component: ManagePreemptionComponent;
  let fixture: ComponentFixture<ManagePreemptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagePreemptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePreemptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
