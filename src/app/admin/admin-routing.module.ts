import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './components/home/home.component';
import { ManageParcelComponent } from './components/manage-parcel/manage-parcel.component';
import { ManageDriverComponent } from './components/manage-driver/manage-driver.component';
import { ManagePreemptionComponent } from './components/manage-preemption/manage-preemption.component';
import { SummaryComponent } from './components/summary/summary.component';
import { TrackStatusComponent } from './components/track-status/track-status.component';
import { TrackingComponent } from './components/tracking/tracking.component';

const routes: Routes = [
  // { path: 'home', component: HomeComponent },
  // { path: 'manage-product', component: ManageProductComponent },
  // { path: '**', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'manage-parcel', component: ManageParcelComponent },
      { path: 'manage-driver', component: ManageDriverComponent },
      { path: 'manage-preemption', component: ManagePreemptionComponent },
      { path: 'summary', component: SummaryComponent },
      { path: 'track-status', component: TrackStatusComponent },
      { path: 'tracking', component: TrackingComponent},
      {path:'**', redirectTo:'home'}
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
