export interface Sender {
  senderAddress: string;
  senderDatetime: Date;
  senderEmail: string;
  senderId: number;
  senderIdPassport: number;
  senderName: string;
  senderTel: number;
}
