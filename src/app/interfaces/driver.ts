export interface Driver {
  driverId: number;
  driverCode: string;
  vehicleRegistrationNumber: number;
  driverName: string;
  driverTel: number;
  driverModel: string;
  driverAddress: string;
}

