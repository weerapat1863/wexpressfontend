import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
  }
  Loginfunction() {
    console.log("login")
    console.log(this.password)
    this.loginService.login(this.username, this.password).subscribe(res => {
      if (res == true)
        this.router.navigate(["admin"])
    })
    // this.loginService.getConfig().subscribe(res => {
    //   console.log(res)
    // })
  }

}
