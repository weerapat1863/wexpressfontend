import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule'},
  // { path: 'admin', loadChildren: './admin/admin.module#AdminModule',canActivate:[true] },
  { path: 'sender', loadChildren: './sender/sender.module#SenderModule' },
  { path: 'driver', loadChildren:'./driver/driver.module#DriverModule'},
  { path: '**', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
